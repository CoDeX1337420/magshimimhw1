#include <iostream>
#include "queue.h"


/*
The function checks if the queue is now full or not.

input: the first element.
output: 1 if full and 0 if not.
*/
bool isFull(queue* q)
{
  if(q->back >= q->size -1 )
  {
    return 1;
  }
  return 0;
}

/*
The function checks if the queue is empty or not.

input: the first element.
output: 1 if empty and 0 if not
*/
bool isEmpty(queue* q)
{
  if(-1 == q->back)
  {
    return 1;
  }
  return 0;
}

/*
The function initiates the whole queue dinamically.

input:the first element of queue
output: NONE
*/
void initQueue(queue* q, unsigned int size)
{
  q->size = size;
  q->front = -1;
  q->back = -1;
  q->vArr = new unsigned int[size];
}

/*
The function adds an element to the end of the queue.

input: the first element and the new value to add.
output:NONE
*/
void enqueue(queue* q, unsigned int newValue)
{
  if (isFull(q))
  {
    std::cout << "The Queue is full." << '\n';
  }
  else
  {
    q->front = 0;
    q->back = q->back + 1;
    q->vArr[q->back] = newValue;
  }
}

/*
The function deletes the first element at the front of teh queue.

inut: the first element
output: -1 if the queue is empty and the  value of the new first element.
*/
int dequeue(queue* q)
{
  int i = 0;
  if (isEmpty(q))
  {
    std::cout << "The Queue is empty." << '\n';
    return -1;
  }

  for (i = 0; i < q->back - 1; i++)
  {
    q->vArr[i] = q->vArr[i+1];
  }
  q->back = q->back - 1;

  return q->vArr[q->front];
}

/*
The functions remove all the elements from the queue.

input: the first element.
output: NONE
*/
void cleanQueue(queue* q)
{
  while(!(isEmpty(q)))
  {
    dequeue(q);
  }
  delete [] q->vArr;
}
